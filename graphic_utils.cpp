#include <vector>
#include <utility>
#include <iostream>

#include <SDL.h>
#include "ignores/cleanup.h"


struct Visualizer {
    SDL_Window* window = NULL;
    SDL_Renderer* renderer = NULL;
};

struct PointVector {
    std::vector<SDL_Point> points = {};
};

struct LineVector {
    std::vector<std::pair<SDL_Point, SDL_Point>> lines = {};
};




inline void addPoint(PointVector &vec, int x, int y) {
    vec.points.push_back((SDL_Point){x, y});
}

inline void addLine(LineVector &vec, int x1, int y1, int x2, int y2) {
    vec.lines.push_back(std::make_pair((SDL_Point){x1, y1}, (SDL_Point){x2, y2}));
}

/**
* Log an SDL error with some error message to the output stream of our choice
* @param os The output stream to write the message to
* @param msg The error message to write, format will be msg error: SDL_GetError()
*/
void logSDLError(std::ostream &os, const std::string &msg){
	os << msg << " error: " << SDL_GetError() << std::endl;
}

void initVisualizer(Visualizer &vis, int px_width, int px_height) {
    if (SDL_Init(SDL_INIT_VIDEO) == 0) {
        SDL_Window* window = NULL;
        SDL_Renderer* renderer = NULL;

        if (SDL_CreateWindowAndRenderer(px_width, px_height, 0, &window, &renderer) == 0) {
            vis.window = window;
            vis.renderer = renderer;
            SDL_SetRenderDrawColor(vis.renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);
            SDL_RenderClear(vis.renderer);
        }
        else {
            cleanup(renderer, window);
        }
    }
}

void cleanupVisualizer(Visualizer &vis) {
    cleanup(vis.renderer, vis.window);
}

void drawLine(Visualizer &vis, SDL_Point from, SDL_Point to) {
    SDL_SetRenderDrawColor(vis.renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderDrawLine(vis.renderer, from.x, from.y, to.x, to.y);
}

void drawLinesUnconnected(Visualizer &vis, std::vector<std::pair<SDL_Point, SDL_Point>> lines) {
    for (auto pair : lines) {
        drawLine(vis, pair.first, pair.second);
    }
}

//Extra line draw is to be able to pass a list of unique points
void drawShape(Visualizer &vis, std::vector<SDL_Point> points) {
    SDL_SetRenderDrawColor(vis.renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    if (points.size() > 0){
        SDL_RenderDrawLines(vis.renderer, &points[0], points.size());
        SDL_RenderDrawLine(vis.renderer, points.front().x, points.front().y, points.back().x, points.back().y);
    }
    else {
        logSDLError(std::cout, "DrawShape, empty shape");
    }
}
