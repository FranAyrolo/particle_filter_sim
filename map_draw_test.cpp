//#include <utility>
#include "graphic_utils.cpp"

int main() {
    
    Visualizer my_vis;
    initVisualizer(my_vis, 800, 600);
    
    LineVector my_lines = {};
    addLine(my_lines, 320, 200, 300, 240);
    addLine(my_lines, 300, 240, 340, 240);
    addLine(my_lines, 340, 240, 320, 200);
    
    PointVector shape = {};
    addPoint(shape, 100, 100);
    addPoint(shape, 200, 100);
    addPoint(shape, 150, 200);
    
    drawLinesUnconnected(my_vis, my_lines.lines);
    drawShape(my_vis, shape.points);
    
    SDL_RenderPresent(my_vis.renderer);
    
    SDL_Delay(2000);
    
    cleanupVisualizer(my_vis);
    SDL_Quit();
    
    return 0;
}
