#include "particles.h"


int main() {
    
    RandomNumberGenerator RNG = new RandomNumberGenerator();
    Visualizer vis = new Visualizer();
    Map map = new Map("filename");
    ParticleFleet fleet = new ParticleFleet(100);
    Particle robot = new Particle((int)map.dimensions.x / 2, (int)map.dimensions.y / 2);
    vector<Measurements> mes_vec;
    vector<double_t> weights;
    vector<uint> resample_vec;
    
    for (int i = 0; i < 10; i++) {
        fleet.disperse(direction, RNG);
        
        mes_vec = fleet.predict(map);
        
        weights = fleet.calculate_weights(measurements, robot.sense);
        //normalize weights
        
        resample_vec = resample(weights_normalizados);
        
        fleet.reinstance(resample_vec);
        vis.pointShow(fleet.getEstimatedPosition());
    }
    
    return EXIT_SUCCESS;
}
