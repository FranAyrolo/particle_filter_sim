struct vec2D {
    double x = 0.0, y = 0.0;
};


class Particle {
public:
    Particle() = default;
    Particle(vec2D pos, vec2D ori);
    void move(vec2D direction, int distance, double noise = 0.0);
    
private:
    vec2D position, orientation;
    double * measurements = nullptr;
    unsigned int n_measures = 0;
};


class ParticleFleet {
public:
    ParticleFleet() = default;
    ParticleFleet(unsigned int n_parts);
    void addParticle(Particle * particle);
    void moveFleet(vec2D direction, int distance, bool with_noise = false);
    vec2D estimatePosition();
    
private:
    Particle * particles = nullptr;
    unsigned int n_particles = 0;
};
