

class Map {
public:
    pair<unsigned int, unsigned int> getDimensions();
    void getDimensions(unsigned int &height, unsigned int &width);
    void setDimensions(unsigned int & const height,unsigned int & const width);
private:
    pair<unsigned int, unsigned int> dimensions = {0, 0};
    unsigned int n_objects = 0;
    vector<Segment> lines = {};
    vector<Landmark> landmarks = {};
    
};
